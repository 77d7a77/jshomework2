let userAnyNumb = +prompt("Enter any number");
while (!Number.isInteger(userAnyNumb)) {
    userAnyNumb = +prompt("Enter any number");
}

if (userAnyNumb >= 5) {
    for (let i = 0; i <= userAnyNumb; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
} else {
    console.log("Sorry, no numbers");
};

// ----Усложненное задание----
// let numberM = +prompt("Enter any number M");
// let numberN = +prompt("Enter any number N больше М");
// while (numberM > numberN) {
//     numberM = +prompt("Enter any number M");
//     numberN = +prompt("Enter any number N больше М");
// }

// for (let i = numberM; i <= numberN; i++) {
//     let flag = 1;
//     if (i > 2 && i % 2 !== 0) {
//         for (let j = 3; j * j <= i; j = j + 2) {
//             if (i % j === 0) {
//                 flag = 0;
//                 break;
//             }

//         }
//     } else if (i !== 2) flag = 0;
//     if (flag === 1) {
//         console.log(i);
//     }
// }